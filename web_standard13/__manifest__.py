# Copyright 2020 - 2023 MD.
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl.html).

{
    "name": "Standard Backend Theme v13",
    "summary": "Standard Responsive Web Backend User Interface for Odoo v13.0",
    "version": "13.0.1.0",
    "category": "Themes/Backend",
    "images": [
        'static/description/banner.png',
        'static/description/theme_screenshot.png'],
    "website": "",
    "author": "MD",
    "license": "LGPL-3",
    "installable": True,
    "depends": ["web"],
    "development_status": "Production/Stable",
    "qweb": ["static/src/xml/*.xml"],
    "data": ["views/assets.xml"],
    "price": 50,
    'currency': 'EUR',
}
